package com.easy;

import java.util.Arrays;

public class Qn1221_SplitStringinBalancedStrings {

    public static void main(String[] args){

        System.out.println(balancedStringSplit("RLRRLLRLRL")); //4
        System.out.println(balancedStringSplit("RLLLLRRRLR")); //3
        System.out.println(balancedStringSplit("LLLLRRRR")); //1
        System.out.println(balancedStringSplit("RLRRRLLRLL")); //2

    }

    private static int balancedStringSplit(String s){

        // for every char in s, pointer counts for number of R and L
        // if equal, reset both to 0, continue traverse and count
        int result = 0;
        int R_count, L_count; R_count = L_count = 0;
        for(char ch : s.toCharArray()){
            if(ch == 'R') ++R_count;
            else ++L_count;

            if(R_count == L_count){
                ++result;
                R_count = L_count = 0;
            }
        }
        return result;
    }
}
